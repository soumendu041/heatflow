#' Fit Generalised Linear Models Subject to the Heat Flow Penalty Using Cross-Validation
#'
#' Uses cross-validation to find the optimal value of the penalty paremeter
#' lambda and the corresponding estimate of beta.
#'
#' @param X data matrix, rows correspond to observations, columns to variables.
#' @param y response vector.
#' @param G graph containing group information (class `"igraph"`).
#' @param optimiser `"sd"` for subgradient descent, `"cd"` for block
#'  co-ordinate descent.
#' @param method `"RW"` for random-walk based method, `"expLap"` for direct
#'  calculation.
#' @param numCores the number of cores to be used for in-parallel generation
#'  of random walks.
#' @param lambda vector of penalty parameters.
#' @param family generalised linear model family.
#' @param init initial estimate of beta, defaults to `"ridge"`; currently, the
#' other supported values are `"zero"` and `"random"`.
#' @param nfolds number of folds to be used for cross-validation.
#' @param thres if `TRUE`, then a thresholding step is applied to the estimator.
#' @param verbose if `TRUE`, then diagnostic outputs are printed.
#' @return a list containing estimates corresponding to lambda_min
#'  and lambda_1se.
#'
#' @references
#' Ghosh, Subhroshekhar, and Soumendu Sundar Mukherjee. "Learning with latent
#' group sparsity via heat flow dynamics on networks."
#' arXiv preprint arXiv:2201.08326 (2022).
#'
#' @examples
#' set.seed(57)
#'
#' ## Generate data
#' n <- 200
#' p <- 100
#' ngrps <- 4
#' grps <- rep(1:ngrps, p * c(80, 120, 200, 100) / 500)
#' grpsizes <- unname(table(grps))
#'
#' # Generate true beta
#' end <- cumsum(grpsizes)
#' start <- c(0, end[1:(ngrps - 1)]) + 1
#' beta <- 1 * runif(p, 0.5, 0.7)
#' for (j in 1:ngrps) {
#'     if (j %% 2 == 0) {
#'         beta[start[j]:end[j]] <- 0
#'     }
#'     if (j == 3) {
#'         beta[start[j]:end[j]] <- runif(grpsizes[j], -0.7, -0.5)
#'     }
#' }
#'
#' # Generate X (block structured covariance matrix)
#' X <- matrix(0, n, p)
#' rho <- c(0.6, 0.9, 0.7, 0.4)
#' equicorr <- function(d, rho) {
#'     stopifnot(rho > -(1 / (d - 1))) # need for the matrix to be PSD
#'     matrix(rho, d, d) + diag(rep(1 - rho, d))
#' }
#' for (j in 1:ngrps) {
#'     mu <- rep(0, grpsizes[j])
#'     Sigma <- equicorr(grpsizes[j], rho[j])
#'     X[, start[j]:end[j]] <- MASS::mvrnorm(n, mu, Sigma)
#' }
#'
#' # Generate y from linear model
#' sigma <- 1 # noise variance
#' y <- X %*% beta + rnorm(n, 0, sigma)
#'
#' ## Estimation
#' # Estimate network/Laplacian by thresholding correlation matrix
#' Sigma_hat <- CovTools::CovEst.2010RBLW(X)$S
#'
#' Dsqrtinv <- diag(1 / sqrt(diag(Sigma_hat)))
#' R <- Dsqrtinv %*% Sigma_hat %*% Dsqrtinv
#'
#' R <- R - diag(diag(R))
#' cutoff <- quantile(c(abs(R)), 0.75)
#'
#' A <- (abs(R) > cutoff) * 1
#' A <- A - diag(diag(A))
#'
#' G <- igraph::graph_from_adjacency_matrix(A, mode = "undirected")
#' L <- igraph::laplacian_matrix(G, normalized = FALSE)
#'
#' heatflow:::plot_matrix(as.matrix(A), legend = FALSE, ticks = FALSE)
#'
#' # Model fitting
#' t_flow <- 0.5
#' lambda <- seq(0.1, 2, by = 0.1)
#'
#' cvfit <- heatflow::cv_heatflow(X, y, G,
#'     t = t_flow,
#'     optimiser = "sd",
#'     method = "RW",
#'     numCores = 2,
#'     lambda = lambda,
#'     family = gaussian(link = "identity"),
#'     init = "ridge",
#'     nfolds = 10,
#'     thres = TRUE
#' )
#'
#' betahat_min <- cvfit$betahat_min_thresholded
#' betahat_1se <- cvfit$betahat_1se_thresholded
#'
#' ## Plot
#' dev.new(width = 10, height = 7)
#' par(cex.axis = 1.5, cex.lab = 1.5)
#' plot(beta, type = "l", col = "black", ylab = "", lty = 2, ylim = c(-2, 2), lwd = 2)
#' lines(1:p, betahat_min, col = "chartreuse3", lwd = 2)
#' lines(1:p, betahat_1se, col = "blue", lwd = 2)
#' legend("topright",
#'     c(
#'         expression(beta),
#'         expression(hat(beta)["Heat flow (min)"]),
#'         expression(hat(beta)["Heat flow (1se)"])
#'     ),
#'     col = c(
#'         "black",
#'         "chartreuse3",
#'         "blue"
#'     ),
#'     lty = c(2, 1, 1),
#'     lwd = rep(2, times = 3),
#'     y.intersp = 1.2,
#'     cex = 1.5
#' )
#' grid()
#'
#' ## Performance metrics
#' print(rbind(
#'     heatflow:::errm_simu(X, beta, betahat_min, name = "heatflow_min"),
#'     heatflow:::errm_simu(X, beta, betahat_1se, name = "heatflow_1se")
#' ))
#'
#' @export
cv_heatflow <- function(X, y, G,
                        t = 0.5,
                        optimiser = "cd",
                        method = "RW",
                        numCores = TRUE,
                        lambda = seq(0.1, 2, by = 0.1),
                        family = gaussian(link = "identity"),
                        init = "ridge",
                        nfolds = 10,
                        thres = TRUE,
                        verbose = FALSE) {
    stopifnot(nrow(X) == length(y))
    n <- nrow(X)
    p <- ncol(X)
    nlam <- length(lambda)

    # Create folds for cross-validation
    perm <- sample(n, replace = FALSE)
    fold_ids <- cut(perm, breaks = nfolds, labels = FALSE)
    folds <- lapply(1:nfolds, function(i) perm[which(fold_ids == i)])

    err <- array(0, c(nlam, nfolds))

    H <- gen_H(t, G, method = method, numCores = numCores)

    betahat <- array(0, c(p, nlam, nfolds))

    for (i in 1:nfolds) {
        val_set <- folds[[i]]
        if (init == "ridge") {
            beta_init <- ridge(X[-val_set, ], y[-val_set], lambda = 0.01)
        } else if (init == "zero") {
            beta_init <- as.matrix(rep(0, p))
        } else if (init == "random") {
            beta_init <- as.matrix(rnorm(p))
        }

        for (u in nlam:1) {
            if (verbose) {
                message("Fold = ", i, " Lambda = ", lambda[u])
            }
            est <- heatflow_solver(X[-val_set, ], y[-val_set], H,
                optimiser = optimiser,
                method = method,
                beta_init = beta_init,
                lambda = lambda[u],
                family = family,
                verbose = verbose
            )$est
            betahat[, u, i] <- est
            err[u, i] <- norm(y[val_set]
            - X[val_set, ] %*% est, "F") / sqrt(length(val_set))
            beta_init <- est # Warm start
        }
    }

    # CV errors
    cv_err <- apply(err, 1, mean)

    min_err <- min(cv_err)
    ind_min <- which(cv_err == min_err)
    lambda_min <- lambda[ind_min]

    ind_1se <- max(which(cv_err <= min(cv_err) + sd(cv_err)))
    lambda_1se <- lambda[ind_1se]

    betahat_min_init <- as.matrix(apply(betahat[, ind_min, ], 1, median))
    betahat_1se_init <- as.matrix(apply(betahat[, ind_1se, ], 1, median))

    betahat_min <- heatflow_solver(X, y, H,
        optimiser = optimiser,
        method, betahat_min_init,
        lambda_min, family
    )$est
    betahat_1se <- heatflow_solver(X, y, H,
        optimiser = optimiser,
        method, betahat_1se_init,
        lambda_1se, family
    )$est
    if (thres) {
        betahat_min_thresholded <- threshold(betahat_min)
        betahat_1se_thresholded <- threshold(betahat_1se)
        out <- list(
            betahat_min = betahat_min,
            betahat_1se = betahat_1se,
            betahat_min_thresholded = betahat_min_thresholded,
            betahat_1se_thresholded = betahat_1se_thresholded
        )
    } else {
        out <- list(
            betahat_min = betahat_min,
            betahat_1se = betahat_1se
        )
    }
    out
}
