heatflow_solver <- function(X, y, H,
                            optimiser = "cd",
                            method = "RW",
                            beta_init,
                            lambda,
                            family = gaussian(link = "identity"),
                            verbose = FALSE) {
    stopifnot(nrow(X) == length(y))

    if (!(family$family %in% c("binomial", "gaussian"))) {
        stop("Family not supported yet!")
    }

    if (!(family$link %in% c("logit", "identity"))) {
        stop("Link function not supported yet!")
    }

    n <- nrow(X)
    p <- ncol(X)

    err <- 1

    if (optimiser == "sd") {
        # subgradient descent
        niter <- min(200, 2 * p)  # maximum number of subgradient steps
        alpha <- rep(0.01, niter)   # learning rate
        tol <- 1e-3 # tolerence

        beta_seq  <- array(0, c(niter, p))
        beta_old <- beta_init

        iter <- 0

        while (iter < niter && (err > tol)) {
            iter <- iter + 1

            h <- as.matrix(hflow(H, (beta_old * beta_old), method))
            zeta <- apply(h, 2, zz)
            w <- X %*% beta_old
            if (family$family == "gaussian" && family$link == "identity") {
                residual <- as.vector(y - w)
            } else if (family$family == "binomial" && family$link == "logit") {
                residual <- as.vector(y - 1 / (1 + exp(-w)))
            }
            # lossgrad <- - rowMeans(t(X) %*% diag(residual))
            lossgrad <- - t(X) %*% residual / n
            subgrad <- lossgrad + lambda * (hflow(H, zeta, method)) * beta_old
            beta_current <- beta_old - alpha[iter] * subgrad

            err <- norm(beta_current - beta_old) / max(norm(beta_old), 1e-10)

            beta_seq[iter, ] <- beta_current
            beta_old <- beta_current
        }
    } else if (optimiser == "cd") {
        ## coordinate descent
        niter <- min(200, 2 * p)  # maximum number of subgradient steps
        alpha <- rep(0.01, niter)   # learning rate
        # alpha <- 0.1 / sqrt(1:niter)   # learning rate
        tol <- 1e-3 # tolerence

        beta_seq  <- array(0, c(niter, p))
        beta_old <- beta_init

        iter <- 0

        while (iter < niter && (err > tol)) {
            iter <- iter + 1
            # ell <- (iter %% p) + 1 # cycle over indices: 2, 3, ..., p, 1
            ell <- sample(p, min(5, floor(p / 3)), replace = FALSE) # Update some random coordinates
            # ell <- 1:p

            w <- X %*% beta_old
            if (family$family == "gaussian" && family$link == "identity") {
                residual <- as.vector(y - w)
            } else if (family$family == "binomial" && family$link == "logit") {
                residual <- as.vector(y - 1 / (1 + exp(-w)))
            }
            lossgrad <-  - t(X[, ell]) %*% residual / n
            # lossgrad <- - rowMeans(t(X) %*% diag(w))
            subgrad <- lossgrad + lambda * subgrad_hflow(H, beta_old, ell, method = method)
            beta_current <- beta_old
            beta_current[ell] <- beta_old[ell] - alpha[iter] * subgrad

            err <- norm(as.matrix(beta_current[ell] - beta_old[ell])) / max(norm(as.matrix(beta_old[ell])), 1e-10)
            # err <- abs(subgrad)

            beta_seq[iter, ] <- beta_current
            beta_old <- beta_current
            # message(err)
            # message(norm(beta_old))
            # message(iter)
        }
    }

    if (verbose) {
        if (iter == niter) {
            message("Maximum number of iterations reached!")
        } else {
            message("Converged in ", iter, " iterations")
        }
    }

    list(beta_seq = beta_seq, est = beta_old)
}
