convolve_2 <- function(H) {
    B <- ncol(H)
    p <- nrow(H)
    HH <- matrix(0, p, B^2)
    for (i in 1:p) {
        for(j in 1:B) {
            left <- (j - 1) * B + 1
            right <- j * B
            HH[i, left:right] <- H[H[i, j], ]
        }
    }
    HH
}
