plot_matrix <- function(mat, legend = TRUE, ticks = TRUE) {
    n <- nrow(mat)
    p <- ncol(mat)
    val <- c(mat)
    valmaxabs <- max(abs(val))
    if (valmaxabs > 0) {
        mat <- mat / valmaxabs
        valmaxabs <- 1
        val <- c(mat)
    }
    m <- 25
    rc1 <- colorRampPalette(colors = c("red", "white"))(m)
    rc2 <- colorRampPalette(colors = c("white", "blue"))(m)
    rampcols <- c(rc1, rc2)
    rampbreaks <- seq(-1, 1, length.out = (2 * m + 1))
    image(x = 1:n, y = 1:p, mat[n:1, ], col = rampcols, breaks = rampbreaks, xaxt = "n", yaxt = "n", xlab = "", ylab = "")
    if (ticks) {
        xticks <- 1:n
        yticks <- 1:p
        axis(1, at = xticks, labels = xticks, side = 3)
        axis(2, at = yticks, labels = rev(yticks), las = 2)
    }
    if (legend) {
        valmin <- min(val)
        valmax <- max(val)
        a <- min(which(rampbreaks >= valmin))
        b <- max(which(rampbreaks <= valmax))
        if (b - a <= 5) {
            myseq <- a:b
        } else {
            myseq <- seq(a, b, by = 5)
            if (!(b %in% myseq)) {
                myseq <- c(myseq, b)
            }
        }
        if (m %in% myseq) {
            j <- which(myseq == m)
            myseq[j] <- (m + 1)
        }
        myseq_col <- myseq
        if ((2 * m + 1) %in% myseq_col) {
            myseq_col[length(myseq)] <- 2 * m
        }
        legend(x = "bottom", inset = c(0, -0.08), legend = round(rampbreaks[myseq], digits = 3), fill = rampcols[myseq_col], xpd = TRUE, horiz = TRUE, bty = "n")
    }
}
