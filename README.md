# Fit Generalised Linear Models subject to the heat flow penalty

We provide **R** implementations of the algorithms proposed in [1].


## Installation

```r
# Install the development version from GitLab:
# install.packages("devtools")
devtools::install_gitlab("soumendu041/heatflow")
```

## Example

```r
library("heatflow")
example("cv_heatflow")
```

## References

1. Ghosh, S. and Mukherjee, S. S. [Learning with latent group sparsity via heat flow dynamics on networks](https://arxiv.org/abs/2201.08326). arXiv:2201.08326 (2022).
